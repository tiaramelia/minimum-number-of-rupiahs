
import request from 'superagent'
class Currency {

    constructor (options = {}) {
        const {url, baseUrl} = options

        this.baseUrl = baseUrl
        this.url = url
    }

    fetchDataCurrency (code) {
        return new Promise((resolve, reject) => {
            request
                .get(this.baseUrl + "/latest?base=" + code )
                .end((err, res) => {
                    if (err) {
                        reject(Error.getErrors(err, res))
                    } else {
                        resolve(res.body)
                    }
                })
        })
    }
}

export default Currency