
import CONFIG from '../config'
import Currency from './currency';
import 'bootstrap/dist/css/bootstrap.css'


class App {
    constructor (options = {}) {
        const {baseUrl} = options
        this.baseUrl = baseUrl


        this.currency = new Currency({
            baseUrl:this.baseUrl
        })
    }
}

const AppRates = new App({
    baseUrl: window.CONFIG ? window.CONFIG.API_PATH : CONFIG.API_PATH
})

export default AppRates