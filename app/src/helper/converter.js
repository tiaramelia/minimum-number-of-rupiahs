class Converter {

    replaceSpecialChar (number, defaultNumber = "")
    {
        return number.toString().replace(/[ -!@#$%^&*()_+\=\[\]{}':"\\|,<>\/?]/g, defaultNumber)
    }

    replaceAlpa (number, defaultNumber = "")
    {
        return number.toString().replace(/[a-zA-Z]/g, defaultNumber)
    }

    numberConverter (number, defaultNumber = 0, checkMinus = true)
    {
        number = this.replaceSpecialChar(number)
        number = this.replaceAlpa(number)
        if(isNaN(parseFloat(number)))
        {
            return defaultNumber
        }
        if(parseFloat(number) < 0 && checkMinus)
        {return defaultNumber}
        return number
    }

    moneyConverter (money = 0, decimal = 10, defaultMoney = null) {
        money = isNaN(parseFloat(money)) ? 0 : money
        let moneyNumber = this.numberConverter(money, money)
        let convertmoney
        if(moneyNumber != undefined)
        {
            let decimalNumber = moneyNumber.toString().split(".")

            if (decimalNumber[1] != undefined) {
                moneyNumber = parseFloat(moneyNumber).toFixed(decimal)
            } else {
                moneyNumber = parseFloat(moneyNumber).toFixed(2)
            }

            convertmoney = moneyNumber.replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
            if(moneyNumber < 0)
            {convertmoney = convertmoney.replace("-", "(") + ")"}
            else if(moneyNumber === 0) { convertmoney = "-"}
        }
        if(defaultMoney != null && money == 0)
        {
            convertmoney = defaultMoney;
        }
        return convertmoney
    }
}

let converter = new Converter()
export default converter