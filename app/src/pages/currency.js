import React, { Component } from 'react';
import { connect } from 'react-redux';
import {FetchDataCurrency, ChangeInitValue, ShowCurrency, DeleteCurrency} from "../actions/currency"
import deleteicon from "../assets/delete_ico_24.svg"
import '../style.css'
import {Convert} from "../helper"

class Currency extends Component {
    constructor(props){
        super(props);
        this.state = {
            latest_curr: 'USD',
            latest_name: 'United States dollar',
            latest_amount: 10000,
            select_curr:'USD',
            showInputCurrency:false
        }

        this.searchCurr = this.searchCurr.bind(this)
        this.deleteCurr = this.deleteCurr.bind(this)
        this.showCurrency = this.showCurrency.bind(this)
    }

    componentWillMount () {
        this.props.dispatch(FetchDataCurrency("USD"))
        this.props.dispatch(ChangeInitValue(this.state.latest_amount))        
    }
    deleteCurr (value) {
        this.props.dispatch(DeleteCurrency(value))
    }
    searchCurr(){
        this.props.dispatch(FetchDataCurrency(this.state.select_curr))
        this.props.dispatch(ChangeInitValue(this.state.latest_amount))
    }
    showCurrency () {
        this.props.dispatch(ShowCurrency(this.state.inputCurrency))
    }
    renderTableCurrency () {
        let data = this.props.currency.listCurrency
        let base = this.props.currency.model.base
        let dataNotDisplay = this.props.currency.listFilterCurrency.map((curr) =>
        <option value={curr.code}>{curr.name}</option>
    )
        return <div>
            <table className="table-currency"><tbody>
                {data.map((row, index) =>
                <tr key={index}>
                    <td>
                        <label className="label-currname">{row.name}</label><br/>
                        <label className="label-detail-curr">{row.code}</label>
                        <label className="label-amount">{Convert.moneyConverter(this.props.currency.initAmount * row.amount)}</label><br/>
                        <label className="label-detail-curr2">{"1 " + base + " = " + row.code + " " + Convert.moneyConverter(row.amount)}</label>
                    </td>
                    <td><div style={{textAlign:'center', paddingTop:15}} onClick={() => this.deleteCurr(row.code)}>
                        <img src={deleteicon} style={{width:24}}/>
                    </div></td>
                </tr>
            )}
            <tr>
                {this.state.showInputCurrency ? 
                <td colSpan={2}>
                    <select id="input-cur" className="form-control" style={{width:312, float:'left'}} value={this.state.inputCurrency} onChange={(e) => this.setState({inputCurrency:e.target.value})}>
                        <option value="" disabled selected>Select currency</option>
                        {dataNotDisplay}
                    </select>
                    <button className="btn btn-secondary" style={{width:'86px'}} onClick={this.showCurrency}>Submit</button>
                </td> :
                <td colSpan={2}><button className="btn btn-secondary" style={{width:'100%'}} onClick={() => this.setState({showInputCurrency:true})}>+ Add More Currencies</button></td>}
            </tr>
            </tbody></table>
            </div>
    }
    render() {
        let dataOption = this.props.currency.listCurrName.map((x) => 
        <option value={x.code}>{x.name}</option>
    )

        return (
            <div id="coat">
                <div className="container text-center" style={{marginTop: "20px"}}>
                    <table className="table">
                        <tbody>
                            <tr>
                                <td style={{textAlign:'left', verticalAlign:'middle'}}>Choose Currency</td>
                                <td>
                                    <input type='text' value={this.state.latest_amount} className="form-control input-currency" style={{textAlign:'right'}}
                                    onChange={(e) => this.setState({latest_amount:e.target.value})} />
                                    <select className="form-control input-currency" value={this.state.select_curr} onChange={(e) => this.setState({select_curr:e.target.value})}>
                                        {dataOption}
                                    </select>
                                    
                                    <button className="btn btn-primary" style={{float:"left"}} onClick={this.searchCurr}>Search Rate</button>
                                </td>
                            </tr>
                            <tr>
                                <td colSpan={2}>
                                    <div style={{padding:'0px 150px'}}>
                                        <label style={{float:'left', fontStyle:'italic', fontSize:12, color:'#7e8893'}}>{this.state.latest_curr + " - " + this.state.latest_name}</label><br/>
                                        <label style={{fontWeight:'bold', fontSize:16, width:'100%'}}>
                                            <span style={{float:'left'}}>{this.state.latest_curr}</span>
                                            <span style={{float:'right'}}>{Convert.moneyConverter(this.props.currency.initAmount)}</span>
                                        </label>

                                        {this.renderTableCurrency()}
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default connect((store) => store)(Currency);