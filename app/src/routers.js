import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import Currency from './pages/currency'

export default <Router>
    <div>
        <Switch>
            <Route exact path="/" component={Currency} />
            <Route exact path="/currency" component={Currency} />
        </Switch>
    </div>
</Router>